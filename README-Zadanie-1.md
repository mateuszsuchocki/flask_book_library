Skład projektu: Mateusz Suchocki, Szymon Chmiel, Kazik Kochan

Aplikacja wykorzystana do przeprowadzenia procesu CI/CD: Flask Book Library
Wykorzystane rozwiązanie CICD: GitLab

Wynikiem zadania 1 są przeprowadzone następujące zadania:
- Stworzony Pull Request 
- Utworzony proces CI/CD
--- Przeprowadzenie testów jednostkowych dla aplikacji Flask Book Library - funkcje books, customers oraz loans
--- Przeprowadzenie testów SCA przy pomocy aplikacji Safety
--- Przeproawdzenie testów SAST przy pomocy aplikacji Bandit 
--- Przeprowadzenie testów DAST przy pomocy aplikacji ZAP

Dla każdego z "jobs" pojawia się informacja o tym jak przebiegł test. Dwa z nich zwracają "warning", mimo że testy przechodzą pomyślnie. Prawdopodobnie jest to spowodowane faktem, że znaleziono błędy w postaci:
- Dla testów SCA - znalezienie błędów wycieku credentiali
- Dla testów SAST - znalezienie błędów o severity Low oraz High w kodzie
Dla tych dwóch przypadków ustawiono flagę "allow_failure" na wartość "true"



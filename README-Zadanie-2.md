# Projekt TBO 23z
## Zadanie 2

Skład projektu: Mateusz Suchocki, Szymon Chmiel, Kazimierz Kochan

### 2. Weryfikacja działania procesu CICD

Przygotowana została nowa gałąź kodu [vuln](https://gitlab.com/mateuszsuchocki/flask_book_library/-/tree/vuln), w której aplikacja została rozwinięta o dwie wady bezpieczeństwa, prowadzące do możliwości wystąpienia incydentów bezpieczeństwa.

#### Podatności bezpieczeństwa
1. XSS w nowej funkcjonalności pisania recenzji książek. 

Commit: [690d0e56](https://gitlab.com/mateuszsuchocki/flask_book_library/-/commit/690d0e560bfa43050d6210b109d915d9b2d564c4)

Aplikacja została rozszerzona o funkcjonalność pisania recenzji książek. Można wpisać wybrany przez siebie nick, wybrać dostępną książkę oraz napisać recenzję.

![Zakładka Reviews z przykładowymi recenzjami](images/reviews.png)

Kod źródłowy jest podatny na atak XSS. Wpisanie w polu recenzji kodu pozwala na doprowadzenie do incydentu bezpieczeństwa. Jak widać na poniższym zrzucie ekranu, wprowadzenie odpowiednio spreparowanego komentarza pozwala na wykonanie skryptu.

![Atak XSS w zakładce Reviews](images/xss.PNG)


2. Wyciek sekretu, danych logowania na testowe konto administratora aplikacji

Commit: [4a9e324b](https://gitlab.com/mateuszsuchocki/flask_book_library/-/commit/4a9e324b8865dfbc9f69d33c50e7cfb362814bc1)

Do aplikacji został dodany "schowany" (niewidoczny w menu) panel logowania dla administratora aplikacji. Jest on przygotowaniem do dodatkowych funkcjonalności przeznaczonych dla administratora, lecz aktualnie jedynie weryfikuje czy podane konto istnieje w bazie danych i czy hasło się zgadza.

![Strona logowania](images/login-page.png)

Oczywiście dla niepoprawnych danych logowania występuje błąd.

![Błąd logowania](images/login-error.png)

Jako, że link nie jest widoczny na głównej stronie, admin uznał, że nikt na nią nie trafi. Dla swojej wygody, żeby nie zapamiętywać danych logowania, ukrył je w kodzie strony z atrybutem hidden, tak, aby ***nikt*** niepowołany nie zdobył do nich dostępu.

!["Ukryte" dane logowania](images/login-leak.png)

Wyciek takich danych może doprowadzić do incydentu bezpieczeństwa, gdy ktoś odkryje, że aplikacja posiada endpoint /login oraz, że dane logowania widoczne są w kodzie źródłowym strony. Korzystając z nich może zdobyć niepowołany dostęp do rozwijanego panelu administratora.

![Wykorzystanie wycieku danych uwierzytelniających](images/login-h4x0r.png)

#### Weryfikacja procesu CICD

Dla commitów zawierających kod źródłowy wzbogacony o podatności proces CICD zwraca "warning". W procesie CICD dla kodu źródłowego przed wprowadzonymi dodatkami były wykrywane: dla testów SCA - znalezienie błędów wycieku credentiali, dla testów SAST - znalezienie błędów o severity Low oraz High w kodzie. Dla tych dwóch przypadków ustawiono flagę "allow_failure" na wartość "true". Spowodowało to, że security-testing przechodzi mimo ich.

1. XSS

Pipeline gdy dodany został kod strony z recenzjami: [#1151202108](https://gitlab.com/mateuszsuchocki/flask_book_library/-/pipelines/1151202108)

![Pipeline przy podatności XSS](images/pipeline-xss.png)

Test DAST, który powinien wykryć podatność XSS nie zidentyfikował jej. Prawdopodobnie jest tak dlatego, że wymagane pole wyboru książki pozwala wprowadzić tam tylko książki istniejące w systemie. Jeżeli skaner nie przygotował najpierw książek w bazie danych, przesłanie formularza nie powinno być możliwe, co oznacza, że nie mógł sprawdzić, czy w polu Review można wprowadzić wykonywalny kod.

![DAST XSS wynik](images/dast-xss.png)

2. Wyciek sekretów

Pipeline gdy dodany został kod z panelem logowania: [#1152116721](https://gitlab.com/mateuszsuchocki/flask_book_library/-/pipelines/1152116721)

![Pipeline przy wycieku sekretów](images/pipeline-login.png)

SAST wskazał dwie nowe wady: sekret oraz potencjalny SQL Injection. Jest to poprawne zachowanie - sekret nie powinien być zawarty w kodzie HTML strony, a zapytanie SQL nie powinno być string-based. Pipeline w poprzednich krokach został ustawiony tak, aby testy z wynikiem warning przechodziły dalej, jak też stało się w tym przypadku.

```
<div id="issue-1">
<div class="issue-block issue-sev-low">
    <b>hardcoded_password_funcarg: </b> Possible hardcoded password: '0029fb49e995f8946e965ad48d1eaf67b57c514ff1bada7d78990f005854dd40'<br>
    <b>Test ID:</b> B106<br>
    <b>Severity: </b>LOW<br>
    <b>Confidence: </b>MEDIUM<br>
    <b>CWE: </b><a href="https://cwe.mitre.org/data/definitions/259.html" target="_blank">CWE-259</a><br>
    <b>File: </b><a href="project/login/models.py" target="_blank">project/login/models.py</a><br>
    <b>Line number: </b>22<br>
    <b>More info: </b><a href="https://bandit.readthedocs.io/en/1.7.7/plugins/b106_hardcoded_password_funcarg.html" target="_blank">https://bandit.readthedocs.io/en/1.7.7/plugins/b106_hardcoded_password_funcarg.html</a><br>

<div class="code">
<pre>
21	    #only for demonstration of data leak in comment on login.html pagenew_user = Login(username=default_username, password=default_password)
22	    db.session.add(Login(username=&#x27;tbo-admin&#x27;, password=&#x27;0029fb49e995f8946e965ad48d1eaf67b57c514ff1bada7d78990f005854dd40&#x27;))
23	    db.session.commit()
</pre>
</div>


</div>
</div>

<div id="issue-2">
<div class="issue-block issue-sev-medium">
    <b>hardcoded_sql_expressions: </b> Possible SQL injection vector through string-based query construction.<br>
    <b>Test ID:</b> B608<br>
    <b>Severity: </b>MEDIUM<br>
    <b>Confidence: </b>LOW<br>
    <b>CWE: </b><a href="https://cwe.mitre.org/data/definitions/89.html" target="_blank">CWE-89</a><br>
    <b>File: </b><a href="project/login/views.py" target="_blank">project/login/views.py</a><br>
    <b>Line number: </b>31<br>
    <b>More info: </b><a href="https://bandit.readthedocs.io/en/1.7.7/plugins/b608_hardcoded_sql_expressions.html" target="_blank">https://bandit.readthedocs.io/en/1.7.7/plugins/b608_hardcoded_sql_expressions.html</a><br>

<div class="code">
<pre>
30	    
31	    query = f&quot;SELECT * FROM users WHERE username=&#x27;{username}&#x27; AND password=&#x27;{password}&#x27;&quot;
32	    query = text(query)
</pre>
</div>
```


#### Podsumowanie
Testy zostały przeprowadzone zgodnie z pipelinem CICD. Wprowadzono podatności. XSS nie został wskazany, a secret leakage oraz potencjalne SQLi pojawiło się w wynikach. Zgodnie z potokiem wyniki zostały potraktowane jako warning, a zbudowanie aplikacji powiodło się.